from commit access *;
from branch access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 5;
real y = 3;
real y_offset = 3.5;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (16,0);
pair bob_pos = (25,0);
pair alice_comment_pos = alice_pos + (-2,12);

real width=1;
real height=1.1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen text_pen_white = text_pen + white;
pen text_pen_bold = Helvetica("b") + fontsize(22);
pen commands_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen target_pen=linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);
pen title_pen = Helvetica("b") + fontsize(30);

Commit[] commits_no_ff_base = {
    Co((alice_pos.x+0*x_offset, alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y), "F", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_no_ff_base.length; ++i)
{
    commits_no_ff_base[i].parents = new Commit[]{commits_no_ff_base[i-1]};
}
commits_no_ff_base[3].parents = new Commit[]{commits_no_ff_base[1]};

Branch[] branches_no_ff_base = {
    Br(commits_no_ff_base[2].position + (x_offset*1.25,0), "master", commits_no_ff_base[2], width=3, height=height),
    Br(commits_no_ff_base[5].position + (x_offset*1.25,0), "feat-x", commits_no_ff_base[5], width=3, height=height),
};

real rebase_offset_y = -8.5;
Commit[] commits_no_ff_rebase = {
    Co((alice_pos.x+0*x_offset, rebase_offset_y+alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, rebase_offset_y+alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, rebase_offset_y+alice_pos.y), "F", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, rebase_offset_y+alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen, shaded=true),
    Co((alice_pos.x+3*x_offset, rebase_offset_y+alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen, shaded=true),
    Co((alice_pos.x+4*x_offset, rebase_offset_y+alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen, shaded=true),
    Co((alice_pos.x+3*x_offset, rebase_offset_y+alice_pos.y+y_offset/2), "C'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, rebase_offset_y+alice_pos.y+y_offset/2), "D'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+5*x_offset, rebase_offset_y+alice_pos.y+y_offset/2), "E'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_no_ff_rebase.length; ++i)
{
    commits_no_ff_rebase[i].parents = new Commit[]{commits_no_ff_rebase[i-1]};
}
commits_no_ff_rebase[3].parents = new Commit[]{commits_no_ff_rebase[1]};
commits_no_ff_rebase[6].parents = new Commit[]{commits_no_ff_rebase[2]};

Branch[] branches_no_ff_rebase = {
    Br(commits_no_ff_rebase[2].position + (x_offset*1.25,0), "master", commits_no_ff_rebase[2], width=3, height=height),
    Br(commits_no_ff_rebase[8].position + (0,y_offset/1.5), "feat-x", commits_no_ff_rebase[8], width=3, height=height),
};


pair head_position = branches_no_ff_base[1].position + (0, y_offset/1.5);
pair head_rebase_position = branches_no_ff_rebase[1].position + (-x_offset*1.5, 0);

// bounding box
pair bbox1 = (-1,-0.7);
pair bbox2 = (17.5,2);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

// command box
pair command_box_start_point = (-1, -11);
real command_box_width = 18.25;
real command_box_height = 1.6;
path command_box = command_box_start_point -- (command_box_start_point + (command_box_width, 0)) --
    (command_box_start_point + (command_box_width, -command_box_height)) --
    (command_box_start_point + (0, -command_box_height)) -- cycle;
fill(command_box, gray(0.9));
draw(command_box, linewidth(2));

real command_y_offset = -1;
void commands(string[] commands) {
  for (int i = 0; i < commands.length; ++i) {
    label("\$~" + commands[i], command_box_start_point + (0.3,-0.8+i*command_y_offset), commands_pen, align=RightSide);
  }
}

label("Avant", (-1, 2), title_pen, align=RightSide);
draw((command_box_start_point.x,-3) -- (command_box_start_point.x+command_box_width,-3), linewidth(7));


save();
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (0, -0.5)) -- (branches_no_ff_base[1].position + (0, 0.6)), linewidth(1), EndArrow);
commands(new string[] {
  "git checkout feat-x",
});
shipout("git-rebase" + string(frame)); frame += 1;
restore();

label("Après", (-1, -4.25), title_pen, align=RightSide);
save();
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (0, -0.5)) -- (branches_no_ff_base[1].position + (0, 0.6)), linewidth(1), EndArrow);
commands(new string[] {
  "git rebase master",
});
draw_branches(branches_no_ff_rebase, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_rebase, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_rebase_position, text_pen_bold);
draw((head_rebase_position + (1.2, 0)) -- (branches_no_ff_rebase[1].position + (-1.5, 0)), linewidth(1), EndArrow);
shipout("git-rebase" + string(frame)); frame += 1;
//restore();
