from commit access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 2.1;
real y = 3;
real y_offset = 2;
real y_offset0 = 3;
real y_offset_content = 0.8;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (13,0);
pair bob_pos = (25,0);
pair alice_comment_pos = alice_pos + (1,2);
pair bob_comment_pos = bob_pos + (-2,2);

real width=1;
real height=1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen content_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit[] alice_commits = {
    Co((alice_pos.x    , alice_pos.y+y_offset0+0*y_offset), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+1*y_offset), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+2*y_offset), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+3*y_offset), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+4*y_offset), "E", null, shaded=true, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x, alice_pos.y+y_offset0+4*y_offset), "E'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x, alice_pos.y+y_offset0+5*y_offset), "F", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x, alice_pos.y+y_offset0+6*y_offset), "G", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x, alice_pos.y+y_offset0+5*y_offset), "H", null, shaded=true, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x, alice_pos.y+y_offset0+6*y_offset), "F'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x, alice_pos.y+y_offset0+7*y_offset), "G'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < alice_commits.length; ++i)
{
    alice_commits[i].parents = new Commit[]{alice_commits[i-1]};
}
alice_commits[5].parents = new Commit[]{alice_commits[3]};
alice_commits[8].parents = new Commit[]{alice_commits[5]};

Commit[] bob_commits = {
    Co((bob_pos.x, bob_pos.y+y_offset0+0*y_offset), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((bob_pos.x, bob_pos.y+y_offset0+1*y_offset), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((bob_pos.x, bob_pos.y+y_offset0+2*y_offset), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((bob_pos.x, bob_pos.y+y_offset0+3*y_offset), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((bob_pos.x, bob_pos.y+y_offset0+4*y_offset), "E'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((bob_pos.x, bob_pos.y+y_offset0+5*y_offset), "H", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < bob_commits.length; ++i)
{
    bob_commits[i].parents = new Commit[]{bob_commits[i-1]};
}

Commit[] server_commits = {
    Co((server_pos.x, server_pos.y+y_offset0+0*y_offset), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+1*y_offset), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+2*y_offset), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+3*y_offset), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+4*y_offset), "E'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+5*y_offset), "H", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+6*y_offset), "F'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x, server_pos.y+y_offset0+7*y_offset), "G'", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < server_commits.length; ++i)
{
    server_commits[i].parents = new Commit[]{server_commits[i-1]};
}

Label[] comments = {
    Label("Alice crée un dépôt local", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice travaille en local", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice envoie son travail", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice continue son travail", alice_comment_pos, text_pen, align=RightSide),
    Label("Bob arrive", bob_comment_pos, text_pen, align=LeftSide),
    Label("Bob travaille", bob_comment_pos, text_pen, align=LeftSide),
    Label("Bob envoie son travail", bob_comment_pos, text_pen, align=LeftSide),
    Label("Alice se synchronise", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice gère l'intégration", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice envoie son travail", alice_comment_pos, text_pen, align=RightSide),
};

// bounding box
pair bbox1 = (0,0);
pair bbox2 = (26.7,18);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

string alice = graphic("../img/alice.eps", "height=3cm");
string server = graphic("../img/server.eps", "height=3cm");
string bob = graphic("../img/bob.eps", "height=3cm");

label(alice, alice_pos);
label("Alice", alice_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

label(server, server_pos);
label("Serveur Git", server_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), EndArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:8], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), EndArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

label(bob, bob_pos);
label("Bob", bob_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:8], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git clone", align=LeftSide, content_pen), (server_pos + (3.5,0)) -- (bob_pos - (1.5,0)), linewidth(1), EndArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:8], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:8], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push", align=LeftSide, content_pen), (server_pos + (3.5,0)) -- (bob_pos - (1.5,0)), linewidth(1), BeginArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:9], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git fetch", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), BeginArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

alice_commits[6].update_shaded(true);
alice_commits[7].update_shaded(true);
alice_commits[8].update_shaded(false);
alice_commits[9].update_shaded(false);
alice_commits[10].update_shaded(false);
save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:11], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("decentralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw_commits(alice_commits[:11], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:8], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(bob_commits[:6], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), EndArrow);
shipout("decentralized-scm" + string(frame)); frame += 1;
