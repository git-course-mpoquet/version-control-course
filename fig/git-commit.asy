from commit access *;
from branch access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 5;
real y = 3;
real y_offset = 2;
real y_offset0 = 3;
real y_offset_content = 0.8;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (16,0);
pair bob_pos = (25,0);
pair comment_pos = (-2,-4);

real width=4;
real height=1.1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen text_pen_white = text_pen + white;
pen text_pen_bold = Helvetica("b") + fontsize(32);
pen text_pen_bold22 = Helvetica("b") + fontsize(22);
pen monospace_pen = Courier() + fontsize(22);
pen index_pen = Courier() + fontsize(22);
pen commands_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen target_pen=linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit[] alice_commits = {
    Co((alice_pos.x    , alice_pos.y+y_offset0+0*y_offset), "5ba6158", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+1*y_offset), "3dcfae1", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+2*y_offset), "27a9a7d", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+3*y_offset), "103c18c", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < alice_commits.length; ++i)
{
    alice_commits[i].parents = new Commit[]{alice_commits[i-1]};
}

pair branch_offset = (5, 0);
Branch[] alice_branches = {
    Br(alice_commits[0].position + branch_offset, "master", alice_commits[0], width=3, height=height),
};

Label[] comments = {
    Label("Création d'un dépôt local", comment_pos, text_pen, align=RightSide),
    Label("Création de fichiers initiaux", comment_pos, text_pen, align=RightSide),
    Label("Ajout des fichiers dans l'index", comment_pos, text_pen, align=RightSide),
    Label("Création du commit", comment_pos, text_pen, align=RightSide),
    Label("Travail sur hello.py et README.md", comment_pos, text_pen, align=RightSide),
    Label("Ajout des fichiers dans l'index", comment_pos, text_pen, align=RightSide),
    Label("Oups : retrait de modifications non souhaitées", comment_pos, text_pen, align=RightSide),
    Label("Création du commit", comment_pos, text_pen, align=RightSide),
    Label("Ajout dans l'index", comment_pos, text_pen, align=RightSide),
    Label("Création du commit", comment_pos, text_pen, align=RightSide),
    Label("Travail sur hello.py", comment_pos, text_pen, align=RightSide),
    Label("Ajout dans l'index", comment_pos, text_pen, align=RightSide),
    Label("Création du commit", comment_pos, text_pen, align=RightSide),
};

// history / index / wd separators
real sep_begin = -2.5;
real sep_history_index = 7;
real sep_index_wd = 20;
real sep_end = 32;
real section_title_y = 12;
pen separator_pen = linetype(new real[] {3,3}) + linewidth(5);

pen wd_pen = Courier() + fontsize(24);
pair wd_start_point = (sep_index_wd + 0.5, 9);
real wd_y_offset = -1;

// bounding box
pair bbox1 = (sep_begin,-5);
pair bbox2 = (sep_end, 13);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

draw((sep_history_index,2.5) -- (sep_history_index,10), separator_pen);
draw((sep_index_wd,2.5) -- (sep_index_wd,10), separator_pen);
label("Historique", ((sep_begin+sep_history_index)/2, section_title_y), text_pen_bold);
label("Index", ((sep_history_index+sep_index_wd)/2, section_title_y), text_pen_bold);
label("Répertoire de travail", ((sep_index_wd+sep_end)/2, section_title_y), text_pen_bold);

// command box
pair command_box_start_point = (-2, 0);
real command_box_width = 23;
real command_box_height = 3;
path command_box = command_box_start_point -- (command_box_start_point + (command_box_width, 0)) --
    (command_box_start_point + (command_box_width, -command_box_height)) --
    (command_box_start_point + (0, -command_box_height)) -- cycle;
fill(command_box, gray(0.9));
draw(command_box, linewidth(2));

pair index_start_point = (sep_history_index + 0.5, 10);
real index_content_width = 12;
real index_x_offset = 0.8;
real index_y_offset = -0.8;

void index_box(real y, real height) {
    pair a = index_start_point + (0, y+0.5);
    pair b = index_start_point + (index_content_width, y+height);
    path box = (a.x, a.y) -- (a.x, b.y) -- (b.x, b.y) -- (b.x, a.y) -- cycle;
    fill(box, gray(0.98));
    draw(box, dashed);
}
void index_chunk(real y, string filename, string[] lines) {
  label(filename, index_start_point+(0,y+ 0*index_y_offset), index_pen, align=RightSide);
  for (int i = 0; i < lines.length; ++i) {
    label(lines[i], index_start_point+(index_x_offset,y+(i+1)*index_y_offset), index_pen, align=RightSide);
  }
}

void index_license(real y) {
  index_box(y, 4*index_y_offset);
  index_chunk(y, "LICENSE", new string[] {
    "new file mode 100644",
    "@@ -0,0 +1 @@",
    "+Public domain.",
  });
}

void index_readme0(real y) {
  index_box(y, 4*index_y_offset);
  index_chunk(y, "README.md", new string[] {
    "new file mode 100644",
    "@@ -0,0 +1 @@",
    "+TODO: README",
  });
}
void index_readme1(real y) {
  index_box(y, 4*index_y_offset);
  index_chunk(y, "README.md", new string[] {
    "@@ -1 +1 @@",
    "-TODO: README",
    "+Hello world codes.",
  });
}

void index_hello_py0(real y) {
  index_box(y, 5*index_y_offset);
  index_chunk(y, "hello.py", new string[] {
    "new file mode 100644",
    "@@ -0,0 +1,2 @@",
    "+\#!/usr/bin/python3",
    "+print('Hello world!')",
  });
}

void index_hello_py1(real y) {
  index_box(y, 6.8*index_y_offset);
  index_chunk(y, "hello.py", new string[] {
    "old mode 100644",
    "new mode 100755",
    "@@ -1,2 +1,2 @@",
    "-\#!/usr/bin/python3",
    "+\#!/usr/bin/env python3",
    "print('Hello world!')",
  });
}


real command_y_offset = -1;
void commands(string[] commands) {
  for (int i = 0; i < commands.length; ++i) {
    label("\$~" + commands[i], command_box_start_point + (0.3,-0.8+i*command_y_offset), commands_pen, align=RightSide);
  }
}

real wd_y_offset = -1;
void wd(string[] files) {
  for (int i = 0; i < files.length; ++i) {
    label(files[i], wd_start_point + (0,+i*wd_y_offset), wd_pen, align=RightSide);
  }
}

pair head_offset = (0,-2);
pair head_position = (0,0);
void update_master_head(Commit c) {
  alice_branches[0].target = c;
  alice_branches[0].position = c.position + branch_offset;
  alice_branches[0].update_border(width=3, height=height);
  head_position = alice_branches[0].position + head_offset;
};
void head() {
  label("HEAD", head_position, text_pen_bold22);
  pair a = head_position + (0, 0.4);
  pair c = alice_branches[0].position + (0,-0.6);
  pair b = (a.x + 0.5, (a.y+c.y)/2);
  draw(a -- c, linewidth(1), EndArrow);
}

real arrow_start_y = 3;
real arrow_offset_x = -1;
real arrow_end_y = 2;
real arrow_bending = -2;
void arrow_to_index() {
  pair a = ((sep_index_wd + sep_end)/2, arrow_start_y);
  pair c = (sep_index_wd + arrow_offset_x, arrow_end_y);
  pair b = midpoint(a -- c) + (0,arrow_bending);
  draw(a .. b .. c, linewidth(3), EndArrow);
}
void arrow_to_history() {
  pair a = ((sep_history_index + sep_index_wd)/2, arrow_start_y);
  pair c = (sep_history_index + arrow_offset_x, arrow_end_y);
  pair b = midpoint(a -- c) + (0,arrow_bending);
  draw(a .. b .. c, linewidth(3), EndArrow);
}
void arrow_to_wd() {
  pair a = ((sep_history_index + sep_index_wd)/2, arrow_start_y);
  pair c = (sep_index_wd - arrow_offset_x, arrow_end_y);
  pair b = midpoint(a -- c) + (0,arrow_bending);
  draw(a .. b .. c, linewidth(3), EndArrow);
}

// --------------

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git init",
});
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "vim LICENSE",
  "vim README.md",
});
wd(new string[] {
  "LICENSE~~~(untracked)",
  "README.md~(untracked)",
});
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git add LICENSE README.md",
});
wd(new string[] {
  "LICENSE~~~(staged)",
  "README.md~(staged)",
});
index_license(0);
index_readme0(-4.25);
arrow_to_index();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git commit -m 'initial commit'",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(clean)",
});
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[0]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
arrow_to_history();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "vim hello.py",
  "vim README.md",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(modified)",
  "hello.py~~(untracked)",
});
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[0]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git add hello.py README.md",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(staged)",
  "hello.py~~(staged)",
});
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[0]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
index_hello_py0(0);
index_readme1(-5);
arrow_to_index();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git restore --staged README.md",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(modified)",
  "hello.py~~(staged)",
});
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[0]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
index_hello_py0(0);
arrow_to_wd();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git commit -m 'python example'",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(modified)",
  "hello.py~~(clean)",
});
draw_commits(alice_commits[:2], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[1]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
arrow_to_history();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git add README.md",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(staged)",
  "hello.py~~(clean)",
});
draw_commits(alice_commits[:2], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[1]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
index_readme1(0);
head();
arrow_to_index();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git commit -m 'doc: README'",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(clean)",
  "hello.py~~(clean)",
});
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[2]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
arrow_to_history();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "vim hello.py",
  "chmod +x hello.py",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(clean)",
  "hello.py~~(modified)",
});
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[2]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git add hello.py",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(clean)",
  "hello.py~~(staged)",
});
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[2]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
index_hello_py1(0);
arrow_to_index();
shipout("git-commit" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
commands(new string[] {
  "git commit -m 'fix python example: \#!, mod+x'",
});
wd(new string[] {
  "LICENSE~~~(clean)",
  "README.md~(clean)",
  "hello.py~~(clean)",
});
draw_commits(alice_commits[:4], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
update_master_head(alice_commits[3]);
draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
head();
arrow_to_history();
shipout("git-commit" + string(frame)); frame += 1;
//restore();


/*draw_branches(alice_branches, text_pen=text_pen, target_pen=target_pen);
label("HEAD", alice_branches[0].position + (4,0), text_pen_bold22);
draw((alice_branches[0].position + (2.8,0)) -- (alice_branches[0].position + (1.5,0)), linewidth(1), EndArrow);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
index_license(0);
index_hello_py1(-4);
commands(new string[] {
  "git init",
});

wd(new string[] {
  ".",
  "|--~LICENSE",
  "|--~README.md",
  "|--~hello.py",
});
shipout("git-commit" + string(frame)); frame += 1;
//restore();
*/
