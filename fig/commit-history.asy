from commit access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real y = 0;
real y_offset = 2.5;
real x_offset = 3;
pair label_offset = (2,0);

real width=1.5;
real height=1.5;
real bend_offset=0.3;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen content_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit c1 = Co((0,y), "v1", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c2 = Co((0,y), "v2", new Commit[]{c1}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c3 = Co((0,y), "v3", new Commit[]{c2}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c4 = Co((0,y), "v4", new Commit[]{c3}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c5 = Co((0,y), "v5", new Commit[]{c4}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen);
Commit c5b = Co((x_offset,y), "v5'", new Commit[]{c4}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c6 = Co((x_offset,y), "v6", new Commit[]{c5b}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c7 = Co((x_offset,y), "v7", new Commit[]{c6}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit[] commits;

Label[] labels = {
    Label("LICENSE", c1.position + label_offset, content_pen, align=RightSide),
    Label("rapport.md, fig/fig1.png", c2.position + label_offset, content_pen, align=RightSide),
    Label("rapport.md", c3.position + label_offset, content_pen, align=RightSide),
    Label("rapport.md, fig/fig2.png", c4.position + label_offset, content_pen, align=RightSide),
    Label("rapport.md", c5.position + label_offset, content_pen, align=RightSide),
};

void draw_labels(Label[] labels)
{
    for (Label l : labels)
    {
        label(l);
    }
}

// bounding box
pair bbox1 = (0,0);
pair bbox2 = (14,16);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

save();
commits.push(c1);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:1]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c2);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:2]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c3);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:3]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c4);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c5);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:5]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
c5.update_shaded(true);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
draw(c5.position+(width/2+bend_offset,0) ..
       c4.position+(width+bend_offset,y_offset/2) ..
       c4.position+(width/2+bend_offset,0),
     black+linewidth(1.5), ArcArrow);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c5b);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c6);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
commits.push(c7);
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
draw(c6.border, linewidth(3));
draw((c6.position + (1.5,0)) .. (c6.position + (3.5,0)), linewidth(3));
draw((c6.position + (2.5,0)) .. (c6.position + (3.5,0)), linewidth(3), Arrow);
label(Label("snapshot.tgz", c6.position + (4,0), content_pen, align=RightSide));
shipout("commit-history" + string(frame)); frame += 1;
restore();

save();
draw_commits(commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_labels(labels[:4]);
draw(c7.border, linewidth(3));
draw(c5b.border, linewidth(3));
draw((c5b.position + (width/2+bend_offset,0)) .. (c6.position + (4,0)) .. (c7.position + (width/2+bend_offset,0)), linewidth(2), ArcArrows);
label(Label("diff", c6.position + (4.5, 0), content_pen, align=RightSide));
shipout("commit-history" + string(frame)); frame += 1;
restore();
