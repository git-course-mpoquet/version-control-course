from commit access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 10;
real y = 3;
real y_offset = 2.5;
real y_offset_content = 0.8;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (12,0);
pair comment_pos = (-3.5,13);
pair content_pos = (-3,5);
path content_box = (content_pos.x, content_pos.y+y_offset_content) -- (4, content_pos.y+y_offset_content) --
                   (4, content_pos.y-3*y_offset_content) --
                   (content_pos.x, content_pos.y-3*y_offset_content) -- cycle;

real width=1.8;
real height=1.5;
real bend_offset=0.3;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen content_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit c1 = Co((server_pos.x,y), "rev 1", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c2 = Co((server_pos.x,y), "rev 2", new Commit[]{c1}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c3 = Co((server_pos.x,y), "rev 3", new Commit[]{c2}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c4 = Co((server_pos.x,y), "rev 4", new Commit[]{c3}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen); y += y_offset;
Commit c5 = Co((server_pos.x,y), "rev 5", new Commit[]{c4}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen);
Commit[] commits = {c1, c2, c3, c4, c5};

Label[] comments = {
    Label("Le serveur gère l'historique", comment_pos+(19,0), text_pen, align=LeftSide),
    Label("Alice veut accéder au dépôt", comment_pos, text_pen, align=RightSide),
    Label("Alice fait une copie locale ", comment_pos, text_pen, align=RightSide),
    Label("Alice modifie/ajoute des fichiers", comment_pos, text_pen, align=RightSide),
    Label("Alice crée une nouvelle version", comment_pos, text_pen, align=RightSide),
    Label("Alice modifie un fichier", comment_pos, text_pen, align=RightSide),
    Label("Alice crée une nouvelle version", comment_pos, text_pen, align=RightSide),
};

Label content_rapport1 = Label("~~rapport.md", content_pos, content_pen, align=RightSide);
Label content_rapport2 = Label("*~rapport.md", content_pos, content_pen, align=RightSide);
Label content_fig1_1 = Label("~~fig/fig1.png", content_pos-(0,y_offset_content), content_pen, align=RightSide);
Label content_fig1_2 = Label("*~fig/fig1.png", content_pos-(0,y_offset_content), content_pen, align=RightSide);
Label content_fig2_1 = Label("~~fig/fig2.png", content_pos-(0,y_offset_content*2), content_pen, align=RightSide);
Label content_fig2_2 = Label("*~fig/fig2.png", content_pos-(0,y_offset_content*2), content_pen, align=RightSide);

// bounding box
pair bbox1 = (-3.5,-3);
pair bbox2 = (16,14);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

string alice = graphic("../img/alice.eps", "height=3cm");
string server = graphic("../img/server.eps", "height=3cm");

label(server, server_pos);
label("Serveur SVN", server_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_commits(commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

label(alice, alice_pos);
label("Alice", alice_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_commits(commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

fill(content_box, gray(0.9));
draw(content_box, black);
save();
label(comments[(int)frame-1]);
draw(Label("svn checkout", align=RightSide, content_pen), (server_pos - (3.5,0)) -- (alice_pos + (1.5,0)), linewidth(1), EndArrow);
label(content_rapport1);
label(content_fig1_1);
draw_commits(commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
label(content_rapport2);
label(content_fig1_1);
label(content_fig2_2);
draw_commits(commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw(Label("svn commit", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), Arrows);
label(content_rapport1);
label(content_fig1_1);
label(content_fig2_1);
draw_commits(commits[:4], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
label(content_rapport2);
label(content_fig1_1);
label(content_fig2_1);
draw_commits(commits[:4], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
draw(Label("svn commit", align=LeftSide, content_pen), (alice_pos + (1.5,0)) -- (server_pos - (3.5,0)), linewidth(1), Arrows);
label(content_rapport1);
label(content_fig1_1);
label(content_fig2_1);
draw_commits(commits[:5], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("centralized-scm" + string(frame)); frame += 1;
restore();
