from commit access *;
from branch access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 5;
real y = 3;
real y_offset = 3.5;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (16,0);
pair bob_pos = (25,0);
pair alice_comment_pos = alice_pos + (-2,12);

real width=1;
real height=1.1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen text_pen_white = text_pen + white;
pen text_pen_bold = Helvetica("b") + fontsize(22);
pen commands_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen target_pen=linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);
pen title_pen = Helvetica("b") + fontsize(30);

Commit[] alice_commits = {
    Co((alice_pos.x+0*x_offset, alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < alice_commits.length; ++i)
{
    alice_commits[i].parents = new Commit[]{alice_commits[i-1]};
}

Branch[] alice_branches = {
    Br(alice_commits[1].position + (x_offset*1.25,0), "master", alice_commits[1], width=3, height=height),
    Br(alice_commits[4].position + (x_offset*1.25,0), "feat-x", alice_commits[4], width=3, height=height),
};

// merge commit
real merge_offset_y = -8;
Commit[] commits_mc = {
    Co((alice_pos.x+0*x_offset, merge_offset_y+alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, merge_offset_y+alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+5*x_offset, merge_offset_y+alice_pos.y), "G", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_mc.length; ++i)
{
    commits_mc[i].parents = new Commit[]{commits_mc[i-1]};
}
commits_mc[5].parents = new Commit[]{commits_mc[4], commits_mc[1]};

Branch[] branches_mc = {
    Br(commits_mc[5].position + (0,y_offset/1.5), "master", commits_mc[5], width=3, height=height),
    Br(commits_mc[4].position + (x_offset*1.25,0), "feat-x", commits_mc[4], width=3, height=height),
};

// no merge commit
real merge_offset_y = -8;
Commit[] commits_nmc = {
    Co((alice_pos.x+0*x_offset, merge_offset_y+alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, merge_offset_y+alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_nmc.length; ++i)
{
    commits_nmc[i].parents = new Commit[]{commits_nmc[i-1]};
}

Branch[] branches_nmc = {
    Br(commits_nmc[4].position + (0,y_offset/1.5), "master", commits_nmc[4], width=3, height=height),
    Br(commits_nmc[4].position + (x_offset*1.25,0), "feat-x", commits_nmc[4], width=3, height=height),
};

pair head_position = alice_branches[0].position + (x_offset*1.5, 0);
pair head_mc_position = branches_mc[0].position + (-x_offset*1.5, 0);
pair head_nmc_position = branches_nmc[0].position + (x_offset*1.25, 0);

// bounding box
pair bbox1 = (-1,-0.7);
pair bbox2 = (17.5,2);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

// command box
pair command_box_start_point = (-1, -11);
real command_box_width = 18.25;
real command_box_height = 1.6;
path command_box = command_box_start_point -- (command_box_start_point + (command_box_width, 0)) --
    (command_box_start_point + (command_box_width, -command_box_height)) --
    (command_box_start_point + (0, -command_box_height)) -- cycle;
fill(command_box, gray(0.9));
draw(command_box, linewidth(2));

real command_y_offset = -1;
void commands(string[] commands) {
  for (int i = 0; i < commands.length; ++i) {
    label("\$~" + commands[i], command_box_start_point + (0.3,-0.8+i*command_y_offset), commands_pen, align=RightSide);
  }
}

label("Avant", (8, 2), title_pen);
draw((command_box_start_point.x,-3) -- (command_box_start_point.x+command_box_width,-3), linewidth(7));

// FAST FORWARD CASES

save();
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (-1.2, 0)) -- (alice_branches[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git checkout master",
});
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (-1.2, 0)) -- (alice_branches[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --ff-only feat-x",
});
draw_branches(branches_nmc, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_nmc, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_nmc_position, text_pen_bold);
draw((head_nmc_position + (-1.2, 0)) -- (branches_nmc[0].position + (1.5,0)), linewidth(1), EndArrow);
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (-1.2, 0)) -- (alice_branches[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --ff feat-x",
});
draw_branches(branches_nmc, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_nmc, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_nmc_position, text_pen_bold);
draw((head_nmc_position + (-1.2, 0)) -- (branches_nmc[0].position + (1.5,0)), linewidth(1), EndArrow);
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (-1.2, 0)) -- (alice_branches[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --no-ff feat-x",
});
draw_branches(branches_mc, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_mc, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_mc_position, text_pen_bold);
draw((head_mc_position + (1.2, 0)) -- (branches_mc[0].position + (-1.5,0)), linewidth(1), EndArrow);
shipout("git-merge" + string(frame)); frame += 1;
restore();

// NON FAST FORWARD CASES
Commit[] commits_no_ff_base = {
    Co((alice_pos.x+0*x_offset, alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y), "F", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_no_ff_base.length; ++i)
{
    commits_no_ff_base[i].parents = new Commit[]{commits_no_ff_base[i-1]};
}
commits_no_ff_base[3].parents = new Commit[]{commits_no_ff_base[1]};

Branch[] branches_no_ff_base = {
    Br(commits_no_ff_base[2].position + (x_offset*1.25,0), "master", commits_no_ff_base[2], width=3, height=height),
    Br(commits_no_ff_base[5].position + (x_offset*1.25,0), "feat-x", commits_no_ff_base[5], width=3, height=height),
};


Commit[] commits_no_ff_merge = {
    Co((alice_pos.x+0*x_offset, merge_offset_y+alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, merge_offset_y+alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, merge_offset_y+alice_pos.y), "F", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, merge_offset_y+alice_pos.y-y_offset/2), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+5*x_offset, merge_offset_y+alice_pos.y), "G", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < commits_no_ff_merge.length; ++i)
{
    commits_no_ff_merge[i].parents = new Commit[]{commits_no_ff_merge[i-1]};
}
commits_no_ff_merge[3].parents = new Commit[]{commits_no_ff_merge[1]};
commits_no_ff_merge[6].parents = new Commit[]{commits_no_ff_merge[2], commits_no_ff_merge[5]};

Branch[] branches_no_ff_merge = {
    Br(commits_no_ff_merge[6].position + (0,y_offset/1.5), "master", commits_no_ff_merge[6], width=3, height=height),
    Br(commits_no_ff_merge[5].position + (x_offset*1.25,0), "feat-x", commits_no_ff_merge[5], width=3, height=height),
};

pair head_no_ff_base_position = branches_no_ff_base[0].position + (x_offset*1.25, 0);
pair head_no_ff_merge_position = head_mc_position;

save();
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_base_position, text_pen_bold);
draw((head_no_ff_base_position + (-1.2, 0)) -- (branches_no_ff_base[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git checkout master",
});
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_base_position, text_pen_bold);
draw((head_no_ff_base_position + (-1.2, 0)) -- (branches_no_ff_base[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --no-ff feat-x",
});
draw_branches(branches_no_ff_merge, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_merge, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_merge_position, text_pen_bold);
draw((head_no_ff_merge_position + (1.2, 0)) -- (branches_no_ff_merge[0].position + (-1.5,0)), linewidth(1), EndArrow);
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_base_position, text_pen_bold);
draw((head_no_ff_base_position + (-1.2, 0)) -- (branches_no_ff_base[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --ff feat-x",
});
draw_branches(branches_no_ff_merge, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_merge, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_merge_position, text_pen_bold);
draw((head_no_ff_merge_position + (1.2, 0)) -- (branches_no_ff_merge[0].position + (-1.5,0)), linewidth(1), EndArrow);
shipout("git-merge" + string(frame)); frame += 1;
restore();

save();
label("Après", (8, -4.25), title_pen);
draw_branches(branches_no_ff_base, text_pen=text_pen, target_pen=target_pen);
draw_commits(commits_no_ff_base, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_no_ff_base_position, text_pen_bold);
draw((head_no_ff_base_position + (-1.2, 0)) -- (branches_no_ff_base[0].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git merge --ff-only feat-x",
});
//draw_branches(branches_no_ff_merge, text_pen=text_pen, target_pen=target_pen);
//draw_commits(commits_no_ff_merge, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
//label("HEAD", head_no_ff_merge_position, text_pen_bold);
//draw((head_no_ff_merge_position + (1.2, 0)) -- (branches_no_ff_merge[0].position + (-1.5,0)), linewidth(1), EndArrow);
label("Not possible to fast-forward, aborting.", (8,-7), Courier() + fontsize(21));
shipout("git-merge" + string(frame)); frame += 1;
//restore();
