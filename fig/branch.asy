from commit access *;

struct Branch
{
    // Cartesian coordinates
    pair position;

    // Content
    string name;

    // "Box" around the branch
    path border;

    // Target commit
    Commit target;

    // Colors
    pen fill_pen;
    pen border_pen;

    void update_border(real width=2, real height=2)
    {
        border = (position + (-width/2, -height/2)) -- (position + (width/2, -height/2)) --
                 (position + (width/2, height/2)) -- (position + (-width/2, height/2)) -- cycle;
    }

    void draw(bool draw_name=true, pen text_pen=defaultpen)
    {
        fill(border, fill_pen);
        draw(border, border_pen);

        if (draw_name)
        {
            label(name, position, text_pen);
        }
    }

    void draw_target(pen target_pen=gray(0.5))
    {
        pair begin = intersectionpoint(position -- target.position, border);
        pair end = intersectionpoint(target.position -- position, target.border);

        draw(begin -- end, target_pen, EndArrow);
    }
};

Branch Br(pair position, string name, Commit target,
    real width=2, real height=2,
    pen fill_pen=rgb("b2df8a"), pen border_pen=linewidth(1.5))
{
    Branch b = new Branch;
    b.position = position;
    b.name = name;
    b.target = target;
    b.fill_pen = fill_pen;
    b.border_pen = border_pen;
    b.update_border(width, height);
    return b;
}

void draw_branches(Branch[] branches, bool draw_target=true,
    bool draw_name=true, pen text_pen=defaultpen, pen target_pen=gray(0.5))
{
    if (draw_target)
    {
        for (Branch b : branches)
            b.draw_target(target_pen);
    }

    for (Branch b : branches)
        b.draw(draw_name, text_pen);
}
