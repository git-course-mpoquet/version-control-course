struct Commit
{
    // Cartesian coordinates
    pair position;

    // Content
    string name;

    // "Box" around the commit
    path border;

    // Parent commits
    Commit[] parents;

    // Colors
    pen fill_pen;
    pen border_pen;
    pen shaded_pen;
    bool shaded;

    void update_border(real width=2, real height=2, real bend_offset=0.3)
    {
        border = (position + (-width/2, -height/2)) -- (position + (width/2, -height/2)) ..
                 (position + (width/2 + bend_offset, 0)) ..
                 (position + (width/2, height/2)) -- (position + (-width/2, height/2)) ..
                 (position - (width/2 + bend_offset, 0)) .. cycle;
    }

    void update_shaded(bool shaded)
    {
        this.shaded = shaded;
        if (shaded)
        {
            shaded_pen = gray(0.7);
        }
        else
        {
            shaded_pen = gray(0);
        }
    }

    void draw(bool draw_name=true, pen text_pen=defaultpen)
    {
        fill(border, fill_pen+shaded_pen);
        draw(border, border_pen+shaded_pen);

        if (draw_name)
        {
            label(name, position, text_pen+shaded_pen);
        }
    }

    void draw_parents(pen parents_pen=gray(0.5), pen shaded_parents_pen=gray(0.25))
    {
        for (Commit parent : parents)
        {
            pair begin = intersectionpoint(position -- parent.position, border);
            pair end = intersectionpoint(parent.position -- position, parent.border);

            if (shaded)
            {
                draw(begin -- end, shaded_parents_pen, EndArrow);
            }
            else
            {
                draw(begin -- end, parents_pen, EndArrow);
            }
        }
    }
};

Commit Co(pair position, string name, Commit[] parents,
    bool shaded = false,
    real width=2, real height=2, real bend_offset=0.3,
    pen fill_pen=rgb("a6cee3"), pen border_pen=linewidth(1.5))
{
    Commit c = new Commit;
    c.position = position;
    c.name = name;
    c.parents = parents;
    c.fill_pen = fill_pen;
    c.border_pen = border_pen;
    c.update_shaded(shaded);
    c.update_border(width, height, bend_offset);
    return c;
}

void draw_commits(Commit[] commits, bool draw_parents=true,
    bool draw_name=true, pen text_pen=defaultpen,
    pen parents_pen=gray(0.5), pen shaded_parents_pen=gray(0.25))
{
    if (draw_parents)
    {
        for (Commit c : commits)
            c.draw_parents(parents_pen, shaded_parents_pen);
    }

    for (Commit c : commits)
        c.draw(draw_name, text_pen);
}
