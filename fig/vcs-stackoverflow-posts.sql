-- Can be queried on https://data.stackexchange.com/stackoverflow/query/new
SELECT
    Tags.TagName AS tag, YEAR(Posts.CreationDate) AS year, MONTH(Posts.CreationDate) AS month, Count(*) as postcount
FROM Posts
    INNER JOIN PostTags ON PostTags.PostId = Posts.Id
    INNER JOIN Tags ON PostTags.TagId = Tags.id
WHERE
    Tags.TagName in ('git', 'svn', 'mercurial', 'tfs')
GROUP BY Tags.TagName, YEAR(Posts.CreationDate), MONTH(Posts.CreationDate)
ORDER BY YEAR(Posts.CreationDate), MONTH(Posts.CreationDate), Tags.TagName
