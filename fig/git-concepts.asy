from commit access *;
from branch access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 5;
real y = 3;
real y_offset = 2;
real y_offset0 = 3;
real y_offset_content = 0.8;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (16,0);
pair bob_pos = (25,0);
pair alice_comment_pos = alice_pos + (-2,12);

real width=4;
real height=1.1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen text_pen_white = text_pen + white;
pen text_pen_bold = Helvetica("b") + fontsize(22);
pen content_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen target_pen=linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit[] alice_commits = {
    Co((alice_pos.x    , alice_pos.y+y_offset0+0*y_offset), "5ba6158", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+1*y_offset), "3dcfae1", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x    , alice_pos.y+y_offset0+2*y_offset), "27a9a7d", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < alice_commits.length; ++i)
{
    alice_commits[i].parents = new Commit[]{alice_commits[i-1]};
}

Commit[] server_commits = {
    Co((server_pos.x    , server_pos.y+y_offset0+0*y_offset), "5ba6158", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x    , server_pos.y+y_offset0+1*y_offset), "3dcfae1", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((server_pos.x    , server_pos.y+y_offset0+2*y_offset), "27a9a7d", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < server_commits.length; ++i)
{
    server_commits[i].parents = new Commit[]{server_commits[i-1]};
}

Branch[] alice_branches = {
    Br(alice_commits[0].position + (6,0), "origin/master", alice_commits[0], width=5, height=height),
    Br(alice_commits[0].position + (x,y_offset), "master", alice_commits[0], width=3, height=height),
};

Branch[] server_branches = {
    Br(server_commits[0].position + (x,0), "master", server_commits[0], width=3, height=height),
};

Branch[] tags = {
    Br(alice_commits[2].position + (x, y_offset), "v1.0.0", alice_commits[2], width=3, height=height, fill_pen=rgb("33a02c")),
    Br(server_commits[2].position + (x, y_offset), "v1.0.0", server_commits[2], width=3, height=height, fill_pen=rgb("33a02c")),
};

Label[] comments = {
    Label("Alice crée un dépôt sur GitHub, avec un README", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice fait une copie locale du dépôt", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice travaille", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice travaille (encore)", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice partage sa branche", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice crée un tag", alice_comment_pos, text_pen, align=RightSide),
    Label("Alice partage son tag", alice_comment_pos, text_pen, align=RightSide),
};

// bounding box
pair bbox1 = (-2.5,-2.5);
pair bbox2 = (23,13);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

string alice = graphic("../img/alice.eps", "height=3cm");
string server = graphic("../img/server.eps", "height=3cm");

label(server, server_pos);
label("Serveur Git", server_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw_commits(server_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

label(alice, alice_pos);
label("Alice", alice_pos - (0,y_offset), text_pen);
save();
label(comments[(int)frame-1]);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git clone", align=RightSide, content_pen), (server_pos - (3.5,0)) -- (alice_pos + (1.5,0)), linewidth(1), EndArrow);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
alice_branches[1].target = alice_commits[1];
alice_branches[1].position = alice_commits[1].position + (x, 0);
alice_branches[1].update_border(width=3,height=height);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:2], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
alice_branches[1].target = alice_commits[2];
alice_branches[1].position = alice_commits[2].position + (x, 0);
alice_branches[1].update_border(width=3,height=height);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:1], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
alice_branches[1].target = alice_commits[2];
alice_branches[1].position = alice_commits[2].position + (x, 0);
alice_branches[1].update_border(width=3,height=height);
alice_branches[0].target = alice_commits[2];
alice_branches[0].position = alice_commits[1].position + (6, 0);
alice_branches[0].update_border(width=5,height=height);
server_branches[0].target = server_commits[2];
server_branches[0].position = server_commits[2].position + (x, 0);
server_branches[0].update_border(width=3,height=height);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push origin master", align=RightSide, content_pen), (server_pos - (3.5,0)) -- (alice_pos + (1.5,0)), linewidth(1), BeginArrow);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw_branches(tags[:1], text_pen=text_pen_white, target_pen=target_pen);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
shipout("git-concepts" + string(frame)); frame += 1;
restore();

save();
label(comments[(int)frame-1]);
label("HEAD", alice_branches[1].position + (4,0), text_pen_bold);
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_branches(server_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw_branches(tags[:2], text_pen=text_pen_white, target_pen=target_pen);
draw((alice_branches[1].position + (2.7,0)) -- (alice_branches[1].position + (3/2+0.125,0)), linewidth(1), EndArrow);
draw_commits(alice_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw_commits(server_commits[:3], text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
draw(Label("git push origin v1.0.0", align=RightSide, content_pen), (server_pos - (3.5,0)) -- (alice_pos + (1.5,0)), linewidth(1), BeginArrow);
shipout("git-concepts" + string(frame)); frame += 1;
//restore();
