from commit access *;
from branch access *;

settings.outformat = "pdf";
unitsize(1cm);

real frame = 1;
real x = 5;
real y = 3;
real y_offset = 3.5;
real x_offset = 3;
pair label_offset = (2,0);
pair alice_pos = (0,0);
pair server_pos = (16,0);
pair bob_pos = (25,0);
pair alice_comment_pos = alice_pos + (-2,12);

real width=1;
real height=1.1;
real bend_offset=0.2;
pen fill_pen=rgb("a6cee3");
pen border_pen=linewidth(1.5);
pen text_pen = Helvetica() + fontsize(22);
pen text_pen_white = text_pen + white;
pen text_pen_bold = Helvetica("b") + fontsize(22);
pen commands_pen = Courier() + fontsize(22);
pen parents_pen=gray(0.5)+linewidth(1);
pen target_pen=linewidth(1);
pen shaded_parents_pen=gray(0.85)+linewidth(1);

Commit[] alice_commits = {
    Co((alice_pos.x+0*x_offset, alice_pos.y), "A", new Commit[]{}, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+1*x_offset, alice_pos.y), "B", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+2*x_offset, alice_pos.y), "C", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+3*x_offset, alice_pos.y), "D", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
    Co((alice_pos.x+4*x_offset, alice_pos.y), "E", null, width=width, height=height, bend_offset=bend_offset, fill_pen=fill_pen, border_pen=border_pen),
};
for (int i = 1; i < alice_commits.length; ++i)
{
    alice_commits[i].parents = new Commit[]{alice_commits[i-1]};
}

Branch[] alice_branches = {
    Br(alice_commits[4].position + (x_offset*1.25,0), "master", alice_commits[4], width=3, height=height),
    Br(alice_commits[4].position + (0, y_offset*0.75), "feat-1", alice_commits[4], width=3, height=height),
    Br(alice_commits[0].position + (0, y_offset*0.75), "feat-2", alice_commits[0], width=3, height=height),
    Br(alice_commits[2].position + (0, y_offset*0.75), "feat-3", alice_commits[2], width=3, height=height),
};

pair head_position = alice_branches[0].position + (0, y_offset/1.5);

// bounding box
pair bbox1 = (-2.2,-0.7);
pair bbox2 = (17.5,5.6);
fill(bbox1 -- (bbox1.x, bbox2.y) -- bbox2 -- (bbox2.x, bbox1.y) -- cycle, white);

// command box
pair command_box_start_point = (-2, 5.5);
real command_box_width = 19;
real command_box_height = 1.7;
path command_box = command_box_start_point -- (command_box_start_point + (command_box_width, 0)) --
    (command_box_start_point + (command_box_width, -command_box_height)) --
    (command_box_start_point + (0, -command_box_height)) -- cycle;
fill(command_box, gray(0.9));
draw(command_box, linewidth(2));

real command_y_offset = -1;
void commands(string[] commands) {
  for (int i = 0; i < commands.length; ++i) {
    label("\$~" + commands[i], command_box_start_point + (0.3,-0.8+i*command_y_offset), commands_pen, align=RightSide);
  }
}

save();
draw_branches(alice_branches[:1], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (0, -0.5)) -- (alice_branches[0].position + (0,0.5)), linewidth(1), EndArrow);
commands(new string[] {
  "git checkout master",
});
shipout("git-branch-create" + string(frame)); frame += 1;
restore();

save();
draw_branches(alice_branches[:2], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (0, -0.5)) -- (alice_branches[0].position + (0,0.5)), linewidth(1), EndArrow);
commands(new string[] {
  "git branch feat-1",
});
shipout("git-branch-create" + string(frame)); frame += 1;
restore();

save();
draw_branches(alice_branches[:3], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (0, -0.5)) -- (alice_branches[0].position + (0,0.5)), linewidth(1), EndArrow);
commands(new string[] {
  "git branch feat-2 A",
});
shipout("git-branch-create" + string(frame)); frame += 1;
restore();

save();
draw_branches(alice_branches[:4], text_pen=text_pen, target_pen=target_pen);
draw_commits(alice_commits, text_pen=text_pen, parents_pen=parents_pen, shaded_parents_pen=shaded_parents_pen);
head_position = alice_branches[3].position + (x_offset*1.25, -y_offset/2.5);
label("HEAD", head_position, text_pen_bold);
draw((head_position + (-0.5, 0.4)) -- (alice_branches[3].position + (1.5,0)), linewidth(1), EndArrow);
commands(new string[] {
  "git checkout -B feat-3 C",
});
shipout("git-branch-create" + string(frame)); frame += 1;
//restore();
