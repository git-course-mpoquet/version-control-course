version-control-course
======================

A (french) course about version control and Git.

How to build slides/exercise sheets?
------------------------------------

Just run ``ninja`` (Ninja_ is a lower-level better-designed alternative to Make_).

All required dependencies are in the Nix_ file (``nix-shell -A slides`` to enter into the build environment).

If you do not want to use Nix, here is the dependency list.

- LaTeX_ suite (*e.g.*, `TeX Live`_): write slides
- pandoc_: write LaTeX with sanity
- Ninja_: build system
- Asymptote_: figure generation
- R_ + packages (tidyverse, docopt, timelineS, viridis): figure generation

.. _Asymptote: https://en.wikipedia.org/wiki/Asymptote_(vector_graphics_language)
.. _LaTex: https://en.wikipedia.org/wiki/LaTeX
.. _Make: https://en.wikipedia.org/wiki/Make_(software)
.. _Ninja: https://ninja-build.org/
.. _Nix: https://nixos.org/download.html
.. _pandoc: https://en.wikipedia.org/wiki/Pandoc
.. _R: https://en.wikipedia.org/wiki/R_(programming_language)
.. _TeX Live: https://en.wikipedia.org/wiki/TeX_Live
