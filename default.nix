{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.05.tar.gz";
    sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
  }) {}
}:

let
  jobs = rec {
    inherit pkgs;

    slides = pkgs.stdenv.mkDerivation {
      pname = "git-slides";
      version = "0.1.0";
      nativeBuildInputs = with pkgs; [
        texlive.combined.scheme-full
        pandoc
        ninja
        inkscape
        asymptote
      ] ++ r-shell.buildInputs;
      src = pkgs.lib.sourceByRegex ./. [
        "^build\.ninja$"
        "^fig$"
        "^fig/.*\.asy$"
        "^fig/.*\.R$"
        "^fig/.*\.csv$"
        "^img$"
        "^img/.*\.eps"
        "^img/.*\.svg"
        "^img/.*\.png"
        "^img/.*\.jpg"
        "^slides$"
        "^slides/version-control\.md$"
        "^slides/.*\.sty$"
      ];
      buildPhase = ''
        ninja slides/version-control.pdf
      '';
      installPhase = ''
        mkdir -p $out
        mv slides/version-control.pdf $out/
      '';
    };

    autorebuild-shell = pkgs.mkShell rec {
      name = "autobuild-shell";
      buildInputs = [
        pkgs.entr
      ] ++ slides.nativeBuildInputs;
      shellHook = ''
        ls build.ninja slides/*.md exercises/*.md slides/*.sty fig/*.R fig/*.asy | entr -s "ninja"
      '';
    };

    r-shell = pkgs.mkShell rec {
      name = "r-shell";
      buildInputs = [
        pkgs.R
        pkgs.rPackages.docopt
        pkgs.rPackages.tidyverse
        pkgs.rPackages.timelineS
        pkgs.rPackages.viridis
      ];
    };
  };
in
  jobs
