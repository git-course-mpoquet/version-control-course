---
title: Contrôle de versions et Git
author: Millian Poquet
theme: Estonia
date: 2021-12-09
lang: fr
header-includes:
- |
  ```{=latex}
  \usepackage{subcaption}
  \setbeamertemplate{footline}[frame number]
  \renewcommand\dots{\ifmmode\ldots\else\makebox[1em][c]{.\hfil.\hfil.}\thinspace\fi}
  \hypersetup{colorlinks,linkcolor=,urlcolor=estonian-blue}
  \graphicspath{{../fig/},{../img/}}

  % Fix pandoc shenanigans
  \setbeamertemplate{section page}{}
  \setbeamertemplate{subsection page}{}
  \AtBeginSection{}
  \AtBeginSubsection{}
  ```
---

# Introduction
### Situations récurrentes

```{=latex}
\begin{block}{Problèmes}
\begin{itemize}
  \item Ce rapport était bien mieux hier.
  \item Mon PC a planté\dots
  \item Ce n'est pas le bon fichier de configuration !
  \item Mais qui a écrit ça ? pourquoi ?
  \item Comment appliquer ce patch de sécurité sur ces versions ?
  \item Comment intégrer ces deux changements en même temps ?
\end{itemize}
\end{block}

\begin{block}{Solution ?}
\begin{itemize}
  \item Sauvegardes régulières :
    \only<1>{\texttt{rapport.docx}}\only<2>{\texttt{rapport2.docx}}\only<3>{\texttt{...}}\only<4->{\texttt{rapport\_FINAL2.docx}}
  \item<5-> Partage par clé usb ? mail ? transferts réseau ?
\end{itemize}
\end{block}

\onslide<6>{
\begin{alertblock}{Solution}
Utiliser un gestionnaire de versions !
\end{alertblock}}
```


### Aspects traités par ce cours
- Gestion de version : concepts clés
- Pourquoi utilise-t-on du décentralisé aujourd'hui ?
- Git : kit de survie et utilisation plus avancée
- Bonnes pratiques (qualité, traçabilité, intégration)

# Contrôle de versions
### Définitions

:::: {.block}
#### Contrôle de version
*version control*, *revision control*, *source code management (scm)*

```{=latex}
    \vspace{1mm}
```

- Gérer l'ensemble des versions de un ou plusieurs fichiers
- Devenu important avec l'informatisation
- Marche sur tout type de fichier, marche **bien** sur fichier textuel
- Surtout utilisé sur du code source

::::

```{=latex}
    \vspace{5mm}
```

#### Gestionnaire de version
*version control system (vcs)*

```{=latex}
    \vspace{1mm}
```

- Un logiciel permettant le contrôle de version
- Dédié (*e.g.*, Git) ou non (*e.g.*, dans Wikipedia ou Google Docs)

## Un peu d'histoire
### Un vieux problème\dots
Le besoin d'identifier et contrôler la version d'œuvres est quasiment aussi vieux que l'écriture.
Il croît avec l'amélioration des techniques d'imprimerie.

- Écriture (Mésopotamie), -3000
- Imprimerie xylographique (Chine), -200
- Imprimerie par caractères mobiles (Chine), 1040
- Imprimerie de Gutenberg (Europe), 1454
- Institut International de Bibliographie, 1895

Standards bibliographiques actuels.

- ISBN, 1970 — publications non périodiques
- ISSN, 1971 — publications périodiques
- PubMed, 1996 — recherche en médecine et biologie
- DOI, 2000 — tout objet numérique

### Un vieux problème, même en informatique

Principaux gestionnaires de versions.

- Locaux : SCCS (1972), RCS
- Centralisés : Subversion, CVS, TFS
- Décentralisés : Git, Mercurial, BitKeeper\dots

```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{vcs-history.pdf}
\end{center}
```


## Popularité de Git

### Popularité écrasante de Git
```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{vcs-google-trends.pdf}
\end{center}
```

#### Une référence pour des concurrents
- `Pijul for Git users` (Pijul doc, page 1, section 1)
- `Fossil vs. Git` (Fossil doc, menu principal du site)
- `Azure DevOps` a deux VCS: son moteur *ad hoc* et Git

### Popularité écrasante de Git (Stack Overflow)
```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{vcs-stackoverflow-posts.pdf}
\end{center}
```

### Popularité des forges
```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{forge-google-trends.pdf}
\end{center}
```

### Adoption dans les projets open source

```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{vcs-usage-oss-raw.pdf}
\end{center}
```

### Adoption dans les projets open source (2)

```{=latex}
\begin{center}
    \includegraphics[width=1\textwidth]{vcs-usage-oss.pdf}
\end{center}
```

## Notion d'historique
### Historique des versions
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}
    \centering
    \only< 1>{\includegraphics[width=.64\textwidth]{commit-history1.pdf}}%
    \only< 2>{\includegraphics[width=.64\textwidth]{commit-history2.pdf}}%
    \only< 3>{\includegraphics[width=.64\textwidth]{commit-history3.pdf}}%
    \only< 4>{\includegraphics[width=.64\textwidth]{commit-history4.pdf}}%
    \only< 5>{\includegraphics[width=.64\textwidth]{commit-history5.pdf}}%
    \only< 6>{\includegraphics[width=.64\textwidth]{commit-history6.pdf}}%
    \only< 7>{\includegraphics[width=.64\textwidth]{commit-history7.pdf}}%
    \only< 8>{\includegraphics[width=.64\textwidth]{commit-history8.pdf}}%
    \only< 9>{\includegraphics[width=.64\textwidth]{commit-history9.pdf}}%
    \only<10>{\includegraphics[width=.64\textwidth]{commit-history10.pdf}}%
    \only<11>{\includegraphics[width=.64\textwidth]{commit-history11.pdf}}%
  \end{figure}
\end{overlayarea}
```

## Contrôle de versions centralisé
### Contrôle de versions centralisé — *e.g.*, Subversion
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}
    \centering
    \only<1>{\includegraphics[width=.8\textwidth]{centralized-scm1.pdf}}%
    \only<2>{\includegraphics[width=.8\textwidth]{centralized-scm2.pdf}}%
    \only<3>{\includegraphics[width=.8\textwidth]{centralized-scm3.pdf}}%
    \only<4>{\includegraphics[width=.8\textwidth]{centralized-scm4.pdf}}%
    \only<5>{\includegraphics[width=.8\textwidth]{centralized-scm5.pdf}}%
    \only<6>{\includegraphics[width=.8\textwidth]{centralized-scm6.pdf}}%
    \only<7>{\includegraphics[width=.8\textwidth]{centralized-scm7.pdf}}%
  \end{figure}
\end{overlayarea}
```

### Contrôle de versions centralisé — *e.g.*, Subversion

```{=latex}
\begin{block}{Fonctionnement résumé}
\begin{itemize}
  \item Un serveur \textbf{central} gère les versions
  \item Les utilisateurs sont des clients qui interagissent avec le serveur
  \begin{itemize}
    \item Pour récupérer une version
    \item Pour soumettre une nouvelle version
    \item Pour faire diverses requêtes (différences entre versions\dots)
  \end{itemize}
  \item Par conséquent, l'historique est très linéaire
\end{itemize}
\end{block}

\pause

\begin{alertblock}{Problèmes}
\begin{itemize}
  \item Impossible de créer des versions locales \\ (client sans accès internet, serveur en panne\dots)
  \item Conflits fréquents, intégration délicate \\
        (intégration incluse dans \textit{vraies} modifs d'une feature/bugfix)
  \item Difficile de travailler en parallèle \\ (plusieurs développeurs sur les mêmes fichiers, \\ un développeur qui travaille sur plusieurs features\dots)
\end{itemize}
\end{alertblock}
```

## Contrôle de versions décentralisé
### Contrôle de versions décentralisé — *e.g.*, Git
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}%
  \begin{figure}%
    \only< 1>{\includegraphics[width=1\textwidth]{decentralized-scm1.pdf}}%
    \only< 2>{\includegraphics[width=1\textwidth]{decentralized-scm2.pdf}}%
    \only< 3>{\includegraphics[width=1\textwidth]{decentralized-scm3.pdf}}%
    \only< 4>{\includegraphics[width=1\textwidth]{decentralized-scm4.pdf}}%
    \only< 5>{\includegraphics[width=1\textwidth]{decentralized-scm5.pdf}}%
    \only< 6>{\includegraphics[width=1\textwidth]{decentralized-scm6.pdf}}%
    \only< 7>{\includegraphics[width=1\textwidth]{decentralized-scm7.pdf}}%
    \only< 8>{\includegraphics[width=1\textwidth]{decentralized-scm8.pdf}}%
    \only< 9>{\includegraphics[width=1\textwidth]{decentralized-scm9.pdf}}%
    \only<10>{\includegraphics[width=1\textwidth]{decentralized-scm10.pdf}}%
  \end{figure}%
\end{overlayarea}
```

### Contrôle de versions décentralisé — *e.g.*, Git
```{=latex}
\begin{block}{Fonctionnement résumé}
\begin{itemize}
  \item Chaque utilisateur a un historique local
  \item Se synchroniser = échanger des bouts d'historique
  \item Intégrer = gérer un historique distribué
  \item Le serveur est inutile\footnote{Un dépôt central est très souvent utilisé pour des raisons pratiques : accès persistant, unicité de la "dernière version à jour", documentation, bug tracker\dots}, on peut s'échanger l'historique sans
  \item L'historique est assez arborescent
\end{itemize}
\end{block}

\begin{exampleblock}{Problèmes ?}
\begin{itemize}
  \item Demande une certaine aise en manipulation d'arbres
  \item Bénéfices du contrôle local et centralisé, sans les inconvénients
\end{itemize}
\end{exampleblock}
```

### À quoi sert un gestionnaire de version aujourd'hui ?

```{=latex}
\begin{itemize}
  \item Historique de versions, traçabilité, maintenabilité
  \item Base pour d'autres outils
  \item Système de backup simple
  \item Travail collaboratif
  \begin{itemize}
    \item Intégration traçable des apports de chacun
    \item Base de toutes les méthodologies de dev récentes
  \end{itemize}
\end{itemize}
```

# Git (1)
###

```{=latex}
\tableofcontents[sections=3]
```

## Concepts clés
### Commits
#### Définition
- C'est un état (*snapshot*) de tout le dépôt Git
- C'est l'unité de base de travail en Git
- Tout travail *commité* peut être récupéré[^git-gc] plus tard
- Identifié par un *hash* unique au dépôt — *e.g.*, `e9ed0f`

#### Contenu
- Contenu des fichiers suivis par Git (et leurs métadonnées)
- Un message de commit. Primordial pour traçabilité (humains)
- Référence vers les commits parents — 1, 2, 0, 66[^git-cthulhu-merge]\dots

[^git-gc]: Sauf appel explicite du GC ou suppressions manuelles de fichiers dans `.git`
[^git-cthulhu-merge]: <https://www.destroyallsoftware.com/blog/2017/the-biggest-and-weirdest-commits-in-linux-kernel-git-history>

### Branches, tags et HEAD
#### Branche
- C'est un pointeur **mobile** vers un commit
- Lorqu'on *commite* à partir d'une branche,
  la branche est mise à jour
  (elle pointe vers le nouveau commit)
- Branche par défaut : `master` (`main`)

#### Tag
- C'est un pointeur **constant** vers un commit
- Peut avoir un message associé (*annotated tag*)
- Utile pour marquer un commit particulier — *e.g.*, `v1.0.0`

#### HEAD
- Un pointeur vers le commit *courant*
- Pointe souvent vers une branche
- Peut pointer directement vers un commit (*detached HEAD*)

### Remotes
#### Définition
- Dépôt distant avec lequel on peut se synchroniser
- Contient des branches distantes (*remote branches*)
- La synchronisation se fait le plus souvent via branches distantes

### Illustration des concepts Git
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}
    \centering
    \only<1>{\includegraphics[width=1\textwidth]{git-concepts1.pdf}}%
    \only<2>{\includegraphics[width=1\textwidth]{git-concepts2.pdf}}%
    \only<3>{\includegraphics[width=1\textwidth]{git-concepts3.pdf}}%
    \only<4>{\includegraphics[width=1\textwidth]{git-concepts4.pdf}}%
    \only<5>{\includegraphics[width=1\textwidth]{git-concepts5.pdf}}%
    \only<6>{\includegraphics[width=1\textwidth]{git-concepts6.pdf}}%
    \only<7>{\includegraphics[width=1\textwidth]{git-concepts7.pdf}}%
  \end{figure}
\end{overlayarea}
```

### Les trois arbres Git

Les commandes git peuvent être déroutantes à première vue.

Leur organisation est logique quand on sait que Git manipule trois structures de données en interne.

- Le répertoire de travail — *sandbox* de travail
- L'*index* ou *staging area* — zone tampon pour créer un commit
- L'arbre des commits — contient l'historique

## Premiers pas, commandes Git\dots
###
```{=latex}
\tableofcontents[sections=3,currentsubsection]
```

### Fichiers internes à Git, configuration
Tous les fichiers internes de Git sont stockés dans le répertoire `.git` (à la racine du dépôt).

Git a beaucoup d'options de configuration. \
Elles sont stockées dans des fichiers, dont voici les principaux.

- `${HOME}/.gitconfig` (globale)
- `.gitconfig` à la racine d'un dépôt (spécifique au dépôt)

Éditables directement (`vim ~/.gitconfig`) ou via des commandes Git
(`git config --global alias.chk checkout`).

### Initialisation d'un dépôt Git

```{=latex}
\begin{block}{Création d'un dépôt local vide}
\begin{itemize}
  \item \texttt{git init [<repo>]}
\end{itemize}
\end{block}

\begin{block}{Récupération d'un dépôt existant}
\begin{itemize}
  \item \texttt{git clone <repo-url> [<dir>]}
\end{itemize}

\vspace{3mm}

Options utiles si on ne veut que la dernière version.

\begin{itemize}
  \item \texttt{-\/-single-branch} : synchronise une seule branche
  \item \texttt{-\/-branch <branch>} : spécifie la branche par défaut
  \item \texttt{-\/-depth 1} : 1 seul commit au lieu de tout l'historique
\end{itemize}
\end{block}

\begin{exampleblock}{Exemples}
\begin{itemize}
  \item \texttt{git init \$\{HOME\}/projects/my-awesome-project}
  \item \texttt{git clone https://github.com/python/cpython.git}
\end{itemize}
\end{exampleblock}
```

### Je suis perdu, que faire ?

```{=latex}
\begin{alertblock}{\texttt{git status}}
\begin{itemize}
  \item Montre votre état courant (branche, synchronisation distante...)
  \item Montre votre \textit{staging area}
  \item Montre comment sortir de situations épineuses (\texttt{rebase}...)
\end{itemize}
\end{alertblock}
\footnotesize
```

~~~
On branch master
Your branch is up to date with gitlab/master.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
  modified:   slides/version-control.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
  modified:   fig/branch.asy
~~~

```{=latex}
\normalsize
```

### Création de commits
#### Enregistrement des modifications à commiter
- `git add <path>...`
- `git rm <path>...`

\vspace{3mm}
Options de `git add`.

- `-f`, `--force` : force l'ajout (cf. `.gitignore`)
- `-p`, `--patch` : sélection interactive des blocs à ajouter. \
  **Utile** pour se relire avant de commit une bêtise.

#### Création du commit
- `git commit [-m <message>]`

\vspace{3mm}
Options.

- `-a`, `--all` : ajoute toute modification des fichiers suivis par Git.
  **À éviter** car favorise le commit de blocs indésirables.


### Création de commits : exemple
```{=latex}
\hspace*{-1cm}\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=1.2\textwidth]{git-commit1.pdf}}%
    \only< 2>{\includegraphics[width=1.2\textwidth]{git-commit2.pdf}}%
    \only< 3>{\includegraphics[width=1.2\textwidth]{git-commit3.pdf}}%
    \only< 4>{\includegraphics[width=1.2\textwidth]{git-commit4.pdf}}%
    \only< 5>{\includegraphics[width=1.2\textwidth]{git-commit5.pdf}}%
    \only< 6>{\includegraphics[width=1.2\textwidth]{git-commit6.pdf}}%
    \only< 7>{\includegraphics[width=1.2\textwidth]{git-commit7.pdf}}%
    \only< 8>{\includegraphics[width=1.2\textwidth]{git-commit8.pdf}}%
    \only< 9>{\includegraphics[width=1.2\textwidth]{git-commit9.pdf}}%
    \only<10>{\includegraphics[width=1.2\textwidth]{git-commit10.pdf}}%
    \only<11>{\includegraphics[width=1.2\textwidth]{git-commit11.pdf}}%
    \only<12>{\includegraphics[width=1.2\textwidth]{git-commit12.pdf}}%
    \only<13>{\includegraphics[width=1.2\textwidth]{git-commit13.pdf}}%
  \end{figure}%
\end{overlayarea}
```

### Visualisation de l'historique
#### Arbre des commits
- `git log [<revision range>] [[--] <path>…]`

\vspace{3mm}
Options courantes.

- `--oneline` : concis (1 ligne par commit)
- `--graph` : parenté entre commits
- `--all` : affiche toutes les branches
- Celles de `git show`…

#### Un commit en particulier
- `git show [<commit>…] [[--] <path>…]`

\vspace{3mm}
Options courantes.

- `-p`, `--patch` : modifications du commit
- `--stat` : résumé des modifications par commit

### Déplacement dans l'historique

#### Déplacement total (HEAD + répertoire de travail)
- `git checkout <branch>` : vers `<branch>`
- `git checkout <commit>` : vers `<commit>` (*detached HEAD*)

#### Ne déplacer que HEAD (conserver répertoire de travail)
- `git reset <t-ish>` : vers une branche, un commit, un tag…

### Déplacement dans l'historique : exemple
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=1\textwidth]{git-move-in-history1.pdf}}%
    \only< 2>{\includegraphics[width=1\textwidth]{git-move-in-history2.pdf}}%
    \only< 3>{\includegraphics[width=1\textwidth]{git-move-in-history3.pdf}}%
    \only< 4>{\includegraphics[width=1\textwidth]{git-move-in-history4.pdf}}%
  \end{figure}%
\end{overlayarea}
```


# Git (2)
## Gestion de branches
###
```{=latex}
\tableofcontents[sections=4,currentsubsection]
```

### Créer une branche
#### Sans s'y déplacer (HEAD inchangé)
- `git branch <branch-name> [<start-point>]`

\vspace{3mm}
Options courantes.

- `-f`, `--force` : met à jour `<branch-name>` si elle existe déjà

#### En s'y déplaçant (HEAD pointe vers la nouvelle branche)
- `git checkout -B <branch-name> [<start-point>]`

### Créer une branche : exemple
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=1\textwidth]{git-branch-create1.pdf}}%
    \only< 2>{\includegraphics[width=1\textwidth]{git-branch-create2.pdf}}%
    \only< 3>{\includegraphics[width=1\textwidth]{git-branch-create3.pdf}}%
    \only< 4>{\includegraphics[width=1\textwidth]{git-branch-create4.pdf}}%
  \end{figure}%
\end{overlayarea}
```

### Fusion de branches

- `git merge [-m <msg>] <stuff…>`

Fusionne `stuff` dans `HEAD`. `stuff` peut être :

- Une branche
- Un commit

Peut créer un commit de fusion ou non selon les options :

- `--ff-only` en cas de fast-forward[^fast-forward], met juste `HEAD` à jour \
  (ne crée pas de commit). Sinon, ne fait rien
- `--no-ff` crée toujours un commit de fusion
- `--ff` (défaut) met juste `HEAD` à jour en cas de fast-forward.
  Sinon, crée un commit de fusion

[^fast-forward]: Arrive lorque les commits à intégrer sont directement au-dessus de `HEAD`.

### Fusion de branches : exemple *fast forward*
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=.9\textwidth]{git-merge1.pdf}}%
    \only< 2>{\includegraphics[width=.9\textwidth]{git-merge2.pdf}}%
    \only< 3>{\includegraphics[width=.9\textwidth]{git-merge3.pdf}}%
    \only< 4>{\includegraphics[width=.9\textwidth]{git-merge4.pdf}}%
  \end{figure}%
\end{overlayarea}
```

### Fusion de branches : exemple non *fast forward*
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=.9\textwidth]{git-merge5.pdf}}%
    \only< 2>{\includegraphics[width=.9\textwidth]{git-merge6.pdf}}%
    \only< 3>{\includegraphics[width=.9\textwidth]{git-merge7.pdf}}%
    \only< 4>{\includegraphics[width=.9\textwidth]{git-merge8.pdf}}%
  \end{figure}%
\end{overlayarea}
```

### Changer la base d'une branche

- `git rebase <newbase> [<branch>]`

Change la base de `<branch>` (par défaut `HEAD`), \
de telle sorte que `<newbase>` devienne sa nouvelle base.

- Ré-applique chaque commit par-dessus la nouvelle base
- Permet de réécrire l'historique
- Très utile pour revenir en *fast forward*

Options courantes :

- `-i, --interactive` permet de choisir finement quoi faire de chaque commit

### Changer la base d'une branche : exemple
```{=latex}
\begin{overlayarea}{\textwidth}{\textheight}
  \begin{figure}%
    \only< 1>{\includegraphics[width=.9\textwidth]{git-rebase1.pdf}}%
    \only< 2>{\includegraphics[width=.9\textwidth]{git-rebase2.pdf}}%
  \end{figure}%
\end{overlayarea}
```

# Git (3)
## Synchronisation
###
```{=latex}
\tableofcontents[sections=5]
```

### Récupérer des objets depuis un dépôt distant
```{=latex}
\texttt{git fetch [<repo>]}

\vspace{7mm}

\begin{itemize}
  \item Met à jour vos branches/tags distants
  \item Ne change \textbf{rien} à vos branches locales
  \item $\rightarrow$ Intégration manuelle avec \texttt{merge} ou \texttt{rebase}
\end{itemize}

\vspace{5mm}

\begin{exampleblock}{Exemples}
\begin{itemize}
  \item \texttt{git fetch}
  \item \texttt{git fetch origin}
  \item \texttt{git fetch -\/-all}
\end{itemize}
\end{exampleblock}
```

### Récupérer et intégrer depuis des objets distants
```{=latex}
\texttt{git pull [<repo> [<ref>]]}

\vspace{5mm}
Raccourci pour :
\begin{itemize}
  \item \texttt{fetch} + \texttt{merge} : \texttt{git pull [-\/-ff-only|-\/-no-ff|-\/-ff]}
  \item \texttt{fetch} + \texttt{rebase} : \texttt{git pull -\/-rebase}
\end{itemize}

\vspace{5mm}
Comportement par défaut configurable (\texttt{man git config}).

\vspace{5mm}
\begin{exampleblock}{Exemples}
\begin{itemize}
  \item \texttt{git pull -\/-ff-only origin master}
  \item \texttt{git pull -\/-rebase}
\end{itemize}
\end{exampleblock}
```

### Envoyer vos objets sur un dépôt distant
`git push [<repo> [<ref>...]]`

\vspace{7mm}

Met à jour les `<ref>` distantes (branche, tag...) à partir de vos versions locales.
Options courantes :

- `--force` l'acceptation distante de la nouvelle version de `<ref>`.
  **Très dangereux sur branche publique !**

\vspace{5mm}

```{=latex}
\begin{exampleblock}{Exemples}
\begin{itemize}
  \item \texttt{git push origin master}
  \item \texttt{git push}
\end{itemize}
\end{exampleblock}
```

# Bonnes pratiques
## Numérotation de versions
### Semantic Versioning[^semver]
Système générique de numérotation de versions.

- Expose aux utilisateurs les cassures d'API \
  $\rightarrow$ Réduit le *dependency hell* [^dependency-hell]
- Ensemble de règles strictes à suivre

\vspace{0.5cm}
Utilisé dans les dépôts de paquets des langages.

- Python : PyPI a un système proche – *cf.* PEP440[^pep440]
- D : Dub l'utilise
- Rust : Cargo l'utilise

[^semver]: <https://semver.org>
[^dependency-hell]: <https://en.wikipedia.org/wiki/Dependency_hell>
[^pep440]: <https://www.python.org/dev/peps/pep-0440/>

## Utilisation de Git
### Message de commit

```{=latex}
\begin{center}
  \includegraphics[width=.6\textwidth]{future-to-now.jpg}
\end{center}
\vspace{0.5cm}
```

~~~
Une ligne. Informatif. Concis (< 50 char)

Détails si besoin.
- Une ligne vide avant les détails !
# commentaire
~~~

### `merge` ou `rebase` ?
`rebase`

- Réécrit l'historique \
  $\rightarrow$ **jamais pour écraser branche publique**[^rebase-golden-rule] \
  $\rightarrow$ Pratique en local pour réduire branchements inutiles \
  $\rightarrow$ Pratique pour nettoyer une branche avant de l'intégrer

`merge`

- Conserve le *vrai* parent d'un commit \
  $\rightarrow$ Peut aider à comprendre un commit plus tard

[^rebase-golden-rule]: <https://www.atlassian.com/git/tutorials/merging-vs-rebasing#the-golden-rule-of-rebasing>

## Workflow
### Workflow Git

Modélise comment collaborer via Git.

- Défini par projet (ou par équipe, par entreprise...)

Bonnes idées pour une bonne qualité.

- Maintenir une branche qui *marche* toujours (tests automatisés)
- Maintenir une documentation à jour (changelog)
- Développer features et corrections à part (feature branches)
- Éviter les branches actives à durée de vie longue

$\rightarrow$ Sortir *souvent* une nouvelle version devient facile.

### Exemple de feature branch

\begin{center}
  \includegraphics[width=.8\textwidth]{feature-branch.png}
\end{center}

### CI/CD
\begin{center}
  \includegraphics[width=1.1\textwidth]{gitlab-ci-workflow.png}
\end{center}

### Exos/pratique s'il reste du temps

Exos sur Git. [`https://mpoquet.github.io/teaching.html`](https://mpoquet.github.io/teaching.html)

- Prise en main de Git
- Gestion de branches et de conflits d'inclusion

Intro GitLab CI. [`https://docs.gitlab.com/ee/ci/introduction/index.html`](https://docs.gitlab.com/ee/ci/introduction/index.html)

Sujets connexes

- Contrôle d'environnements logiciels : **[`Nix`](nixos.org/)** `;)`
- Virtualisation app/env/fs : Docker...

# Références
### Remerciements et références

Pour faire ces slides

- Arnaud Legrand — idées de slides de mooc-rr[^mooc-rr]
- Adoption des VCS dans l'open source[^vcs-adoption]

Pour aller plus loin

- Documentation officielle de git[^gitdoc]
- `man git`, `man git checkout`\dots

[^mooc-rr]: <https://gitlab.inria.fr/learninglab/mooc-rr> (module 1, slides)
[^gitdoc]: <https://git-scm.com/doc>
[^vcs-adoption]: <https://mpoquet.github.io/blog/2020-08-vcs-adoption-in-floss/index.html>
